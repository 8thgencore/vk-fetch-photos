import argparse
import random
import logging
import sys
from pathlib import Path

import aiohttp
import aiofiles
import asyncio
import re

from aiohttp import ClientSession
from aioretry import RetryInfo, RetryPolicyStrategy, retry
from bs4 import BeautifulSoup

USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36'
proxy_list = []

logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=logging.DEBUG,
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)

logger = logging.getLogger(__name__)


def retry_policy(info: RetryInfo) -> RetryPolicyStrategy:
    """
    - It will always retry until succeeded
    - If fails for the first time, it will retry immediately,
    - If it fails again,
      aioretry will perform a 100ms delay before the second retry,
      200ms delay before the 3rd retry,
      the 4th retry immediately,
      100ms delay before the 5th retry,
      etc...
    """
    logger.warning(f'{info.exception.__class__}')
    return False, (info.fails - 1) % 3 * 0.1


@retry(retry_policy)
async def fetch(session: ClientSession, url: str) -> str:
    """
    Get avatar image URL and save image in /tmp/vk_img path

    :param session:  ClientSession object
    :param url: vk user page URL
    :return: avatar image URL
    """
    ava_url = await extract_avatar_url(session, url)

    if ava_url is not None:
        await load_avatar(ava_url, session, url)
    return ava_url


@retry(retry_policy)
async def load_avatar(ava_url, session, url):
    async with session.get(ava_url, proxy=random.choice(proxy_list)) as response:
        response_content = await response.read()
        profile_id = re.findall(r'\d+', url)[0]
        filename = Path(args.out_dir) / f'{profile_id}.jpg'
        async with aiofiles.open(filename, 'wb') as file:
            await file.write(response_content)


@retry(retry_policy)
async def extract_avatar_url(session, url):
    async with session.get(url, proxy=random.choice(proxy_list)) as response:
        response_text = await response.text()

        # try extract avatar url by regex
        ava_regex_result = re.findall(r'(https://.*.jpg\?size=400.*ava=1)"', response_text)
        ava_url = ava_regex_result[0].replace("&amp;", "&") if ava_regex_result else None
        if ava_url:
            return ava_url if ava_url.startswith('https') else None

        # try extract avatar url from parsed page
        bs = BeautifulSoup(response_text, 'lxml')
        ava_url_element = bs.select_one('#page_avatar > img, #profile_photo_link > img')
        if ava_url_element:
            ava_url = ava_url_element['src']
            return ava_url if ava_url.startswith('https') else None
        else:
            banned = bs.select_one('#login_blocked_img')
            if banned:
                logger.debug(f'{url} banned')
                return None

            not_created = bs.select_one('#content > div > div.message_page_body')
            if not_created:
                logger.debug(f'{url} not created')
                return None

            hidden_profile_element = bs.select_one('.profile_deleted_text')
            if hidden_profile_element:
                logger.debug(f'{url} hidden profile')
                return None

            logger.warning(f'Cannot extract avatar from {url}')


async def main(i):
    url_generator = (
        f'https://vk.com/id{index}' for index in
        range(
            i * args.pool_size + 1,
            i * args.pool_size + args.pool_size + 1
        )
    )
    tasks = []

    client_headers = {
        'User-Agent': USER_AGENT,
    }

    async with aiohttp.ClientSession(headers=client_headers) as session:
        for url in url_generator:
            tasks.append(fetch(session, url))
        avatars = await asyncio.gather(*tasks)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='vk fetch images arguments')
    parser.add_argument('proxy_list', type=str, help='path to file with proxies')
    parser.add_argument('out_dir', type=str, help='path to out data dir')
    parser.add_argument('start_range', type=int, help='start iteration')
    parser.add_argument('stop_range', type=int, help='stop iteration')
    parser.add_argument('pool_size', type=int, help='pool size')
    args = parser.parse_args()

    with open(args.proxy_list) as proxy_file:
        proxy_list += proxy_file.read().splitlines()

    Path(args.out_dir).mkdir(parents=True, exist_ok=True)

    loop = asyncio.get_event_loop()
    for iteration in range(args.start_range, args.stop_range):
        loop.run_until_complete(main(iteration))
        print(f'iteration {iteration} complete')
